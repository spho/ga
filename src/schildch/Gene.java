package schildch;

import java.util.Random;

public class Gene implements Comparable<Gene> {

	byte[] byteString;
	Random random = new Random(System.nanoTime());
	boolean debug = false;
	int bitSize = 0;
	int nrOfBytes = 0;

	double fitness = Double.NEGATIVE_INFINITY;

	/*
	 * Parameter: -size is in nr of bits
	 * 
	 */
	public Gene(int size) {
		bitSize = size;
		int bytes = size / 8;
		if (size % 8 != 0) {
			bytes++;
		}

		byteString = new byte[bytes];
		nrOfBytes = bytes;
		randomPopulate();

	}

	public void randomPopulate() {

		random.nextBytes(byteString);
	}

	/*
	 * Parameters: -probability range 0 to 1000000 mapped to 0% to 100%
	 * 
	 */
	public void mutate(int probability) {

		if (debug) {
			System.out.println("Bevor mutation: " + (byteString[1] & 0x80));
		}

		int p = 0;
		for (int i = 0; i < byteString.length; i++) {
			for (int j = 0; j < 8; j++) {
				p = random.nextInt(1000001);
				if (p < probability) {
					byteString[i] = (byte) (byteString[i] ^ (0x00 + 1 << j));
				}
			}

		}
		if (debug) {
			System.out.println("After mutation: " + (byteString[1] & 0x80));
		}
	}

	public Gene generateChildren(Gene yourLovelyPartner, boolean bitwise) {
		Gene beautifulChild = new Gene(bitSize);

		int crossoverByte = random.nextInt(byteString.length);
		int crossoverPoint = random.nextInt(8)+1;
		int counter = 0;
		
		
		
		if (bitwise) {
			boolean iAmLikeMyDad = true;
			Gene donnator = this;
			for (int i = 0; i < byteString.length; i++) {
				beautifulChild.byteString[i] = 0x00;
				for (int j = 1; j < 9; j++) {
					iAmLikeMyDad = random.nextBoolean();
					if (iAmLikeMyDad == true) {
						donnator = this;
					} else {
						donnator = yourLovelyPartner;
					}
					switch (j) {
					case (1):
						beautifulChild.byteString[i] |= donnator.byteString[i] & 0x01;
						break;
					case (2):
						beautifulChild.byteString[i] |= donnator.byteString[i] & 0x02;
						break;
					case (3):
						beautifulChild.byteString[i] |= donnator.byteString[i] & 0x04;
						break;
					case (4):
						beautifulChild.byteString[i] |= donnator.byteString[i] & 0x08;
						break;
					case (5):
						beautifulChild.byteString[i] |= donnator.byteString[i] & 0x10;
						break;
					case (6):
						beautifulChild.byteString[i] |= donnator.byteString[i] & 0x20;
						break;
					case (7):
						beautifulChild.byteString[i] |= donnator.byteString[i] & 0x40;
						break;
					case (8):
						beautifulChild.byteString[i] |= donnator.byteString[i] & 0x80;
						break;
					}
				}
				
			}
			

		}else {
			boolean iAmLikeMyDad = true;
			Gene donnator = this;
			for (int i = 0; i < byteString.length; i++) {
				beautifulChild.byteString[i] = 0x00;
				for (int j = 1; j < 9; j++) {
					if(i<crossoverByte&&j<crossoverPoint){
					iAmLikeMyDad = true;}
					else{
						iAmLikeMyDad=false;
					}
					if (iAmLikeMyDad == true) {
						donnator = this;
					} else {
						donnator = yourLovelyPartner;
					}
					switch (j) {
					case (1):
						beautifulChild.byteString[i] |= donnator.byteString[i] & 0x01;
						break;
					case (2):
						beautifulChild.byteString[i] |= donnator.byteString[i] & 0x02;
						break;
					case (3):
						beautifulChild.byteString[i] |= donnator.byteString[i] & 0x04;
						break;
					case (4):
						beautifulChild.byteString[i] |= donnator.byteString[i] & 0x08;
						break;
					case (5):
						beautifulChild.byteString[i] |= donnator.byteString[i] & 0x10;
						break;
					case (6):
						beautifulChild.byteString[i] |= donnator.byteString[i] & 0x20;
						break;
					case (7):
						beautifulChild.byteString[i] |= donnator.byteString[i] & 0x40;
						break;
					case (8):
						beautifulChild.byteString[i] |= donnator.byteString[i] & 0x80;
						break;
					}
				}
				
			}
			

		}

		if (debug) {
			debug_OutputOneByteToConsole(0, "Male");
			debug_OutputOneByteToConsole(1, "Male");

			yourLovelyPartner.debug_OutputOneByteToConsole(0, "Female");
			yourLovelyPartner.debug_OutputOneByteToConsole(1, "Female");

			beautifulChild.debug_OutputOneByteToConsole(0, "Child");
			beautifulChild.debug_OutputOneByteToConsole(1, "Child");

		}

		return beautifulChild;

	}

	public void debug_OutputOneByteToConsole(int nr, String tag) {
		System.out.println("Output of byte nr:" + nr + " Tag: " + tag);
		System.out.print((byteString[nr] & 0x80) >> 7);
		System.out.print((byteString[nr] & 0x40) >> 6);
		System.out.print((byteString[nr] & 0x20) >> 5);
		System.out.print((byteString[nr] & 0x10) >> 4);
		System.out.print((byteString[nr] & 0x08) >> 3);
		System.out.print((byteString[nr] & 0x04) >> 2);
		System.out.print((byteString[nr] & 0x02) >> 1);
		System.out.println((byteString[nr] & 0x01));
	}

	public String getBinaryRepresentation() {
		String r = "";
		for (int i = 0; i < nrOfBytes; i++) {
			r += "'";
			r += ((byteString[i] & 0x80) >> 7);
			r += ((byteString[i] & 0x40) >> 6);
			r += ((byteString[i] & 0x20) >> 5);
			r += ((byteString[i] & 0x10) >> 4);
			r += ((byteString[i] & 0x08) >> 3);
			r += ((byteString[i] & 0x04) >> 2);
			r += ((byteString[i] & 0x02) >> 1);
			r += ((byteString[i] & 0x01));
			r += "';";
		}
		return r;
	}

	public String getByteRepresentation() {
		String r = "";
		for (int i = 0; i < nrOfBytes; i++) {
			r += byteString[i] + ";";
		}
		return r;
	}
	
	private void cropByteArray(int[] array){
		if (bitSize % 8 != 0) {
			int rest = 8 - bitSize % 8;

			if (rest == 1) {
				array[nrOfBytes - 1] = (byte) (array[nrOfBytes - 1] & 0x7F);
			}else if (rest == 2) {
				array[nrOfBytes - 1] = (byte) (array[nrOfBytes - 1] & 0x3F);
			}else if (rest == 3) {
				array[nrOfBytes - 1] = (byte) (array[nrOfBytes - 1] & 0x1F);
			}else if (rest == 4) {
				array[nrOfBytes - 1] = (byte) (array[nrOfBytes - 1] & 0x0F);
			}else if (rest == 5) {
				array[nrOfBytes - 1] = (byte) (array[nrOfBytes - 1] & 0x07);
			}else if (rest == 6) {
				array[nrOfBytes - 1] = (byte) (array[nrOfBytes - 1] & 0x03);
			}else if (rest == 7) {
				array[nrOfBytes - 1] = (byte) (array[nrOfBytes - 1] & 0x01);
			}
			
		}
	}

	public int bitToInt() {
		int value = 0;
		int[] c= new int[nrOfBytes];
		
		for(int i = 0;i<nrOfBytes;i++){
			c[i]=(byteString[i]+128);
		}
		cropByteArray(c);

		for (int i = 0; i < nrOfBytes; i++) {
			value += c[i] *Math.pow(256, i);
		}

		return value;
	}

	@Override
	public int compareTo(Gene evilCompetitor) {

		if (fitness > evilCompetitor.fitness) {
			return -1;
		} else if (fitness < evilCompetitor.fitness) {
			return 1;
		} else {
			return 0;
		}
	}

}
