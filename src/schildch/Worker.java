package schildch;

public class Worker implements Runnable {

	boolean isRunning = true;
	int id = 0;

	public Worker(int id) {
		this.id = id;

	}

	@Override
	public void run() {
		if (!Main.minimalisticOutput) {
			System.out.println("New Worker " + id + " is running");
		}

		while (isRunning) {
			try {
				GA ga = new GA(Main.populationSize, Main.bitStringSize, Main.replace, Main.maxIterations,
						Main.mutateRate, Main.tableUsed, Main.stoppingMeasure, Main.averageSamples,
						Main.skippedMutaion);
				Main.getStat(ga.run());
			} catch (Exception e) {
				System.out.println("System Worker crashed: " + e.toString());
				System.exit(-5);
			}

		}
		if (!Main.minimalisticOutput) {
			System.out.println("Worker " + id + " is terminating");
		}
		Main.callMain();

	}

	public void terminate() {
		isRunning = false;
	}

}
