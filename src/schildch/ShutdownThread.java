package schildch;

public class ShutdownThread implements Runnable {

	@Override
	public void run() {
		
		System.out.println("Average time until Global optima is found with probability of " + Main.certainlyFactor
						+ ";Average Fitnesscalls until Global optima is found with probability of " + Main.certainlyFactor
						+ ";#Global optima Reached;Average time per run;avg. Iterations;"
						+ "nrOfRuns;populationSize;bitStringSize;replace;maxIterations;mutateRate;Skipped Mutation;Average Sample size; Stopping measure;Nr Threads;Nr Cores Available;Memory Used;Max memory for JVM (MB)");

		
		
		System.out.println(Main.averageTimeUntilGlobalOptimaIsCertainlyFound + ";"
					+ Main.averageFitnesscallsUntilOptimaIsCertainlyFound + ";" + Main.nrOfFoundOptima + ";" + Main.averageTimeNeeded
					+ ";" + Main.averageNumberOfIteration + ";" + Main.nrOfRuns + ";"
					+ Main.populationSize + ";" + Main.bitStringSize + ";" + Main.replace + ";" + Main.maxIterations + ";"
					+ (double) (Main.mutateRate) / 10000 + "%" + ";" + Main.skippedMutaion + ";" + Main.averageSamples + ";"
					+ Main.stoppingMeasure + ";" + Main.nrOfThreads + ";" + Runtime.getRuntime().availableProcessors() + ";"
					+ Runtime.getRuntime().totalMemory() / 1024 / 1024 + ";"
					+ Runtime.getRuntime().maxMemory() / 1024 / 1024);
		
	
		
	}

}
