package schildch;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Random;

public class GA {

	ArrayList<Gene> population;

	ArrayList<double[]> dataTank = new ArrayList<double[]>();

	int geneSize = 0;
	int replace = 0;
	Random random = new Random(System.nanoTime());
	int maxGenerations = 0;
	boolean stoppingCriteriaFullfilled = false;
	double stoppingmeasure = 0.99;
	int averageSampleNr = 40;
	int skippedMutation = 3;
	boolean tableUsed = false;

	int mutateRate = 0;

	boolean debug = false;

	double[] lastHoundertBestvals = new double[100];

	/*
	 * Parameters: -fixedPopulation defines the size of the population if fixed
	 * Population is equal to -1 then the populationsize is variable
	 * geneBitssize determines how many bits an bitsting from the gene has
	 */
	public GA(int fixedPopulation, int geneBitsize, int replaceX, int generations, int mutateR, boolean table,
			double stoppingMes, int avgSamp, int skipMut) {

		tableUsed=table;
		stoppingmeasure = stoppingMes;
		averageSampleNr = avgSamp;
		skippedMutation = skipMut;
		geneSize = geneBitsize;
		replace = replaceX;
		maxGenerations = generations;
		mutateRate = mutateR;
		population = new ArrayList<Gene>();

		for (int i = 0; i < fixedPopulation; i++) {
			population.add(new Gene(geneBitsize));
		}

	}

	public StatisticElement run() {

		long startTime = System.nanoTime();
		StatisticElement statistic = new StatisticElement();
		int generaaation = 0;

		for (int i = 0; i < maxGenerations & !stoppingCriteriaFullfilled; i++) {

			generaaation = i;

			// Fitness evaluation
			fitnessEvalustion();

			// Evaluate
			evaluate(i);
			
			//Save Values
			for(int j =0;j<population.size();j++){
			Main.saveMe[generaaation][j]=population.get(j).fitness;
			}

			// Geneartion Children
			generateChildren();

			// Mutate
			mutate();
		}

		statistic.nrOfGenerations = generaaation;
		statistic.bestFitnessValue = population.get(0).fitness;
		statistic.time = ((double) (System.nanoTime() - startTime)) / 1000000;
		statistic.winningGene = population.get(0);

		// Save Statistics
		// saveStatistics();

		return statistic;
	}

	private void saveStatistics() {

		Calendar cal = Calendar.getInstance();

		SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmssZ");

		PrintWriter writer;
		try {
			writer = new PrintWriter("GA_statistics" + sdf.format(cal.getTime()) + ".csv", "UTF-8");
			writer.println("Generation; Values");
			for (int i = 0; i < dataTank.size(); i++) {
				writer.print(i + ";");
				for (int j = 0; j < population.size(); j++) {
					writer.print(dataTank.get(i)[j] + ";");
				}
				writer.println();
			}

			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void fitnessEvalustion() {
		for (int j = 0; j < population.size(); j++) {
			makeFitnessTestandSendThemToTheGym(population.get(j));

		}
	}

	private void evaluate(int generation) {
		

		Collections.sort(population);
		double[] newGenerationValues = new double[population.size()];
		for (int i = 0; i < population.size(); i++) {
			newGenerationValues[i] = population.get(i).fitness;
		}

		dataTank.add(newGenerationValues);

		double bestVal = population.get(0).fitness;

		for (int i = 99; i > 0; i--) {
			lastHoundertBestvals[i] = lastHoundertBestvals[i - 1];
		}
		lastHoundertBestvals[0] = bestVal;

		double topTen = 0;
		double bottomTen = 0;

		for (int i = 0; i < averageSampleNr; i++) {
			topTen += lastHoundertBestvals[i];
			bottomTen += lastHoundertBestvals[averageSampleNr + i];
			
		}
		
		if (bottomTen > lastHoundertBestvals[0]*averageSampleNr * stoppingmeasure) {

			stoppingCriteriaFullfilled = true;
		}

		
		
		if (debug) {
			System.out.println("Best guy in generation " + generation + " with fitness of " + bestVal);
		}
		
		
	}

	private void generateChildren() {
		for (int nrC = 1; nrC < replace + 1; nrC++) {
			int male = random.nextInt(population.size() - replace);
			int female = random.nextInt(population.size() - replace);
			while (male == female) {
				female = random.nextInt(population.size() - replace);
			}
			population.set(population.size() - nrC,
					population.get(male).generateChildren(population.get(female), true));

		}
	}

	private void mutate() {
		for (int j = 0 + skippedMutation; j < population.size(); j++) {
			population.get(j).mutate(mutateRate);

		}
	}

	private void makeFitnessTestandSendThemToTheGym(Gene g) {

		double f = 0;

		/*
		 * for (int i = 0; i < g.byteString.length; i++) { f += g.byteString[i];
		 * }
		 */

		f = eightBitWiseFitnessEvaluation(g);

		g.fitness = f;

	}

	// Needs 48bit
	private double eightBitWiseFitnessEvaluation(Gene g) {
		double fit = 0;

		if(!tableUsed){
	
			double pre = 1;
			double fa = 1;
			double ta = 1;

			if (g.bitSize > 15) {
				pre = 1 / (Math.sinh(g.byteString[0] * g.byteString[1]) + 1);

				if (g.bitSize > 31) {
					fa = Math.cos(g.byteString[2] * g.byteString[1]);
				}
				if (g.bitSize > 47) {
					ta = Math.log10(
							Math.abs(fa / (pre + 0.5)) * Math.abs(g.byteString[3]) + Math.abs(g.byteString[4]) + 10)
							- g.byteString[5];
				}

			}

			fit = pre * fa * ta + ta / (pre + 1) - fa * Math.PI + 1;
	
		}else{
			
			try{
			fit= Main.fitnessTable[g.bitToInt()];
			}catch(Exception e){
				System.out.println("Index: "+g.bitToInt());
				System.exit(-3);
			}
			
			
			
		}
		return fit;

	}

}
