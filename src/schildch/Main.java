package schildch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

/*
 *Goal is to compare the influence of:
 * -the mutation rate 
 * -the population size
 * -elitism 
 * 
 * Measure are: 
 * -the nr of average global optima found within error marrgin of 2% 
 * -average nubers of iteration
 * -average time needed
 * 
 * How long does it take to reach a point in the global optima with a certainty of 99%
 * Assumtion each run is indemendent of its predecessor. So the global optima are randomly distributet
 * with probability p = (#found optima)/(#total runs). 
 * So the probability q that a optima is  found within k runs is q= (1-(1-p)^k )
 * And thus the average time is T=k*(avg time per run)
 * 
 * k=log(1-q)/log(1-p)
 * 
 * So in resume following variables are needed:
 * 
 * s=#found optima
 * r=#total runs
 * t=avg time per run
 *  
 * 
 */

public class Main {

	final static int versionID = 69;

	final static int selectOfFitnessfuntion = 1;
	final static int maxIterations = 100;
	final static int nrOfRuns = 100000;

	static int populationSize = 14;
	static int bitStringSize = 48;
	static int replace = 6;
	static int mutateRate = 150000;
	static int skippedMutaion = 8;
	static double stoppingMeasure = 0.85;
	static int averageSamples = 25;

	static double globalOptima = 1;
	final static StatisticElement[] stat = new StatisticElement[nrOfRuns];
	final static double allowedErrorMarrgin = 0.02;
	static double averageNumberOfIteration = 0;
	static double averageTimeNeeded = 0;
	static double probabilityOfFindingGlobalOptima = 0;
	static double nrOfFoundOptima = 0;
	static double averageTimeUntilGlobalOptimaIsCertainlyFound = 0;
	static double averageFitnesscallsUntilOptimaIsCertainlyFound = 0;
	static double certainlyFactor = 0.99;

	// static GA ga = new GA(populationSize, bitStringSize, replace,
	// maxIterations, mutateRate, selectOfFitnessfuntion);
	static double overalltime = 0;
	static long startTime;

	// Multihread setting
	static int nrOfThreads = 0;
	static int indexOfThreads = 0;
	static int activeThreads = 0;
	static Worker[] worker;
	static boolean onlyOneStopNoRacePlease = true;
	static boolean onlyOneHundert = true;

	// Load the tables
	static double[] fitnessTable = null;
	static boolean tableUsed = false;

	// For Output
	static Calendar cal = Calendar.getInstance();
	static SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmssZ");

	// Variable for looping
	static boolean looping = true;
	static int globalCounter = 1;
	final static int targetGlobalRuns = 1000;
	final static boolean minimalisticOutput = true;
	static Random random;
	static Thread shutdownthread = new Thread(new ShutdownThread());
	
	public static double[][] saveMe = new double[500][14];

	public static void main(String[] args) {

		// Runtime.getRuntime().addShutdownHook(shutdownthread);
		random = new Random(System.nanoTime());
		/*
		modifyParameters();

		if (selectOfFitnessfuntion != 0) {
			tableUsed = true;
			loadFitnessTable();
		}

		start();
		*/
		loadFitnessTable();
		GA ga = new GA(14, 14, 6, 500, 50000, true,0.999, 20,8);
		ga.run();

		
		
		try
		{
		    PrintWriter pr = new PrintWriter("file.txt");    

		    for (int i=0; i<500 ; i++)
		    {
		    	for(int j=0;j<14;j++){
		        pr.print(saveMe[i][j]+";");
		    	}
		    	pr.println();
		    }
		    pr.close();
		}
		catch (Exception e)
		{
		    e.printStackTrace();
		    System.out.println("No such file exists.");
		}

	}

	private static void modifyParameters() {
		/*
		 * Constrains: populationsize>=replace+2 populationsize>skippedMutation
		 * 
		 * Ranges: 2<populationsize<inf (300) 0<replace<populationsize-1
		 * 0<mutationRange<1'000'000 0<=skippedMutation<=populationsize
		 * 0<stoppingMeasure<1 0<averageSamples<50
		 * 
		 * =>675 Billionen "sinnvolle" möglichkeiten als searchspace
		 */

		final int maxMutationInPercent = 30;

		// populationSize=random.nextInt(197)+3;
		
		 populationSize =(int)(Math.abs(random.nextGaussian()*20)+3); 
		 replace= random.nextInt(populationSize-2)+1; 
		 mutateRate=(int)(random.nextDouble()*maxMutationInPercent*10000);
		 skippedMutaion = random.nextInt(populationSize)+1; 
		 stoppingMeasure=Math.max(0.01, 1- 0.3*Math.abs(random.nextGaussian()));
		 averageSamples = random.nextInt(50);
		 
		/*
		populationSize = 72;
		replace = 65;
		mutateRate = 107400;
		skippedMutaion = 45;
		stoppingMeasure = 0.67764854;
		averageSamples = 2;
		*/

		// mutateRate+=10000;
		// populationSize++;
		// replace=populationSize/2;
		// stoppingMeasure-=0.002;

	}

	private static void doneOneCompleteRun() {
		nrOfThreads = 0;
		indexOfThreads = 0;
		activeThreads = 0;
		onlyOneStopNoRacePlease = true;
		onlyOneHundert = true;

		if (targetGlobalRuns == globalCounter) {
			looping = false;
			System.out.println("System Terminated");
		}

		if (looping) {
			modifyParameters();

			globalCounter++;
			start();

		}
	}

	static private void loadFitnessTable() {

		// The name of the file to open.
		String fileName = new File("").getAbsolutePath();

		// This will reference one line at a time
		String line = null;

		switch (selectOfFitnessfuntion) {
		case 1:
			fileName += ("\\tabels\\Bend014.tab");
			globalOptima = 1;
			break;
		case 5:
			fileName += ("\\tabels\\Divider012.tab");
			globalOptima = Math.sqrt(2);
			break;
		case 3:
			fileName += ("\\tabels\\BendExp15010.tab");
			globalOptima = 0.99;
			break;
		case 4:
			fileName += ("\\tabels\\BendExp42010.tab");
			globalOptima = 0.98;
			break;
		case 2:
			fileName += ("\\tabels\\BendExp75010.tab");
			globalOptima = 0.98;
			break;
		default:
			System.out.println("Invalid selcetionOfFitnessfunttion: " + selectOfFitnessfuntion);
			System.exit(-1);
			break;
		}

		try {
			// FileReader reads text files in the default encoding.
			FileReader fileReader = new FileReader(fileName);

			// Always wrap FileReader in BufferedReader.
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			line = bufferedReader.readLine();
			char[] c = line.toCharArray();

			String z1 = "";
			String z2 = "";

			int z = 0;

			for (int i = 0; i < c.length; i++) {
				if (z == 0 && c[i] == ' ') {

				} else if (z == 0 && c[i] != ' ') {
					z1 += c[i];
					z = 1;
				} else if (z == 1 && c[i] != ' ') {
					z1 += c[i];
				} else if (z == 1 && c[i] == ' ') {
					z = 2;
				} else if (z == 2 && c[i] == ' ') {

				} else if (z == 2 && c[i] != ' ') {
					z2 += c[i];
				}

			}

			bitStringSize = Integer.parseInt(z1);
			fitnessTable = new double[(int) Math.pow(2, bitStringSize)];

			while ((line = bufferedReader.readLine()) != null) {
				c = line.toCharArray();

				z1 = "";
				z2 = "";

				z = 0;

				for (int i = 0; i < c.length; i++) {
					if (z == 0 && c[i] == ' ') {

					} else if (z == 0 && c[i] != ' ') {
						z1 += c[i];
						z = 1;
					} else if (z == 1 && c[i] != ' ') {
						z1 += c[i];
					} else if (z == 1 && c[i] == ' ') {
						z = 2;
					} else if (z == 2 && c[i] == ' ') {

					} else if (z == 2 && c[i] != ' ') {
						z2 += c[i];
					}

				}
				if (z1 != "") {
					fitnessTable[Integer.parseInt(z1)] = Double.parseDouble(z2);
				}

			}

			// Always close files.
			bufferedReader.close();
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to open file '" + fileName + "'");
			ex.printStackTrace();
			System.exit(-2);
		} catch (IOException ex) {
			System.out.println("Error reading file '" + fileName + "'");
			System.exit(-2);
			// Or we could just do this:
			// ex.printStackTrace();
		}
	}

	static public synchronized void getStat(StatisticElement st) {
		if (!minimalisticOutput) {
			if (onlyOneHundert) {
				if (indexOfThreads % (nrOfRuns / 100) == 0) {
					System.out.println(
							"Global runs: " + globalCounter + " Status: " + (indexOfThreads * 100) / nrOfRuns + "%");
				}
				if (indexOfThreads == nrOfRuns) {
					onlyOneHundert = false;
				}
			}
		}

		if (nrOfRuns > indexOfThreads) {
			stat[indexOfThreads] = st;
			indexOfThreads++;
		} else {
			for (int i = 0; i < nrOfThreads; i++) {
				worker[i].isRunning = false;

			}
			if (onlyOneStopNoRacePlease) {
				onlyOneStopNoRacePlease = false;
				stop();
			}
		}

	}

	private static void start() {

		nrOfThreads = Runtime.getRuntime().availableProcessors();
		worker = new Worker[nrOfThreads];
		startTime = System.nanoTime();
		for (int i = 0; i < nrOfThreads; i++) {
			worker[i] = new Worker(i);

		}
		for (int i = 0; i < nrOfThreads; i++) {

			Thread t = new Thread(worker[i]);
			t.start();

		}
		if (minimalisticOutput) {
			System.out.println("In Mainiteration nr: " + globalCounter + " of " + targetGlobalRuns);
		}
		activeThreads = nrOfThreads;

	}

	private static void stop() {

		overalltime = ((double) (System.nanoTime() - startTime)) / 1000000000;
		save();
	}

	private static void loadParameters(int nr) {
		// The name of the file to open.
		String fileName = new File("").getAbsolutePath() + "parameter.csv";

		// This will reference one line at a time
		String line = "";

		try {
			// FileReader reads text files in the default encoding.
			FileReader fileReader = new FileReader(fileName);

			// Always wrap FileReader in BufferedReader.
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			for (int i = 0; i < nr + 1; i++) {
				line = bufferedReader.readLine();
			}

			char[] c = line.toCharArray();

			int counter = 0;
			for(int i = 0;counter<7;i++){
				if(c[i]==';'){
					counter++;
				}
			}
			
			String value = "";
			
			
			
			
			
			String z1 = "";
			String z2 = "";

			int z = 0;

			for (int i = 0; i < c.length; i++) {
				if (z == 0 && c[i] == ' ') {

				} else if (z == 0 && c[i] != ' ') {
					z1 += c[i];
					z = 1;
				} else if (z == 1 && c[i] != ' ') {
					z1 += c[i];
				} else if (z == 1 && c[i] == ' ') {
					z = 2;
				} else if (z == 2 && c[i] == ' ') {

				} else if (z == 2 && c[i] != ' ') {
					z2 += c[i];
				}

			}

			bitStringSize = Integer.parseInt(z1);
			fitnessTable = new double[(int) Math.pow(2, bitStringSize)];

			while ((line = bufferedReader.readLine()) != null) {
				c = line.toCharArray();

				z1 = "";
				z2 = "";

				z = 0;

				for (int i = 0; i < c.length; i++) {
					if (z == 0 && c[i] == ' ') {

					} else if (z == 0 && c[i] != ' ') {
						z1 += c[i];
						z = 1;
					} else if (z == 1 && c[i] != ' ') {
						z1 += c[i];
					} else if (z == 1 && c[i] == ' ') {
						z = 2;
					} else if (z == 2 && c[i] == ' ') {

					} else if (z == 2 && c[i] != ' ') {
						z2 += c[i];
					}

				}
				if (z1 != "") {
					fitnessTable[Integer.parseInt(z1)] = Double.parseDouble(z2);
				}

			}

			// Always close files.
			bufferedReader.close();
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to open file '" + fileName + "'");
			ex.printStackTrace();
			System.exit(-2);
		} catch (IOException ex) {
			System.out.println("Error reading file '" + fileName + "'");
			System.exit(-2);
			// Or we could just do this:
			// ex.printStackTrace();
		}

	}

	private static void save() {

		double bestFitness = 0;
		int bestIndex = 0;
		averageNumberOfIteration = 0;
		averageTimeNeeded = 0;
		probabilityOfFindingGlobalOptima = 0;
		nrOfFoundOptima = 0;

		for (int i = 0; i < nrOfRuns; i++) {

			if (stat[i].bestFitnessValue > bestFitness) {
				bestFitness = stat[i].bestFitnessValue;
				bestIndex = i;

			}

			if (stat[i].bestFitnessValue > globalOptima * (1 - allowedErrorMarrgin)) {
				nrOfFoundOptima++;
			}
			averageNumberOfIteration += stat[i].nrOfGenerations;
			averageTimeNeeded += stat[i].time;

		}

		averageNumberOfIteration /= nrOfRuns;
		averageTimeNeeded /= nrOfRuns;
		probabilityOfFindingGlobalOptima = nrOfFoundOptima / nrOfRuns;

		double k = Math.log(1 - certainlyFactor) / Math.log(1 - probabilityOfFindingGlobalOptima);

		if (nrOfFoundOptima == 0) {
			k = 999999;
		}

		k = Math.max(1, k);

		if (k != 0) {
			averageTimeUntilGlobalOptimaIsCertainlyFound = k * averageTimeNeeded;
			averageFitnesscallsUntilOptimaIsCertainlyFound = k * averageNumberOfIteration * populationSize;
		}
		
		write2(bestIndex);
		write3(bestIndex);

	}

	public static synchronized void callMain() {
		activeThreads--;
		if (activeThreads == 0) {
			doneOneCompleteRun();
		}
	}

	private static void write3(int bestIndex) {
		PrintWriter writer;
		try {
			writer = new PrintWriter(new FileWriter("Statistics.csv", true));

			writer.println(averageTimeUntilGlobalOptimaIsCertainlyFound + ";"
					+ averageFitnesscallsUntilOptimaIsCertainlyFound + ";" + nrOfFoundOptima + ";" + averageTimeNeeded
					+ ";" + averageNumberOfIteration + ";" + stat[bestIndex].bestFitnessValue + ";" + nrOfRuns + ";"
					+ populationSize + ";" + bitStringSize + ";" + replace + ";" + maxIterations + ";"
					+ (double) (mutateRate) / 10000 + "%" + ";" + skippedMutaion + ";" + averageSamples + ";"
					+ stoppingMeasure + ";" + nrOfThreads + ";" + Runtime.getRuntime().availableProcessors() + ";"
					+ Runtime.getRuntime().totalMemory() / 1024 / 1024 + ";"
					+ Runtime.getRuntime().maxMemory() / 1024 / 1024);
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void write2(int bestIndex) {
		String path = "Statistics.csv";

		File f = new File(path);
		if (f.exists() && !f.isDirectory()) {
			return;
		} else {

			PrintWriter writer;
			try {
				writer = new PrintWriter(new FileWriter(path, true));

				writer.println("Average time until Global optima is found with probability of " + certainlyFactor
						+ ";Average Fitnesscalls until Global optima is found with probability of " + certainlyFactor
						+ ";#Global optima Reached;Average time per run;avg. Iterations;Best Value;"
						+ "nrOfRuns;populationSize;bitStringSize;replace;maxIterations;mutateRate;Skipped Mutation;Average Sample size; Stopping measure;Nr Threads;Nr Cores Available;Memory Used;Max memory for JVM (MB)");

				writer.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	private static void write1(int bestIndex) {
		PrintWriter writer;
		try {
			writer = new PrintWriter("GA_statistics" + sdf.format(cal.getTime()) + ".csv", "UTF-8");
			writer.println("Overall time in sec;Best fitness;;Winning Gene");
			writer.println(overalltime + ";" + stat[bestIndex].bestFitnessValue + ";;"
					+ stat[bestIndex].winningGene.getBinaryRepresentation());
			writer.println(";;;" + stat[bestIndex].winningGene.getByteRepresentation());
			writer.println();

			writer.println("Settings");
			writer.println(
					"nrOfRuns;populationSize;bitStringSize;replace;maxIterations;mutateRate;Skipped Mutation;Average Sample size; Stopping measure;Nr Threads;Nr Cores Available;Memory Used;Max memory for JVM (MB)");
			writer.println(nrOfRuns + ";" + populationSize + ";" + bitStringSize + ";" + replace + ";" + maxIterations
					+ ";" + mutateRate + ";" + skippedMutaion + ";" + averageSamples + ";" + stoppingMeasure + ";"
					+ nrOfThreads + ";" + Runtime.getRuntime().availableProcessors() + ";"
					+ Runtime.getRuntime().totalMemory() / 1024 / 1024 + ";"
					+ Runtime.getRuntime().maxMemory() / 1024 / 1024);

			writer.println();
			writer.println("Run;Best;Nr of Generations;Time in ms;;Best Gene");
			for (int i = 0; i < nrOfRuns; i++) {
				writer.println(i + ";" + stat[i].bestFitnessValue + ";" + stat[i].nrOfGenerations + ";" + stat[i].time
						+ ";;" + stat[i].winningGene.getBinaryRepresentation()
						+ stat[i].winningGene.getByteRepresentation());
			}

			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
